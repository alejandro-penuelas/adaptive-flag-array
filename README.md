# Adaptive Flag Array

### Adaptive flag array generation (AFAG) method for reversible data hiding systems.

- Reversible data hiding schemes for images require the collection of control data for ensuring reversibility. 
- In particular, those pixels in an image that are prone to be overflowed or underflowed after the watermark embedding process (e.g., because they values are too close to 0 or 255, for 8-bit images) need to be pre-processed before the data embedding stage. 
- The AFAG method provides an efficient mechanism to pre-process images with a high number of pixels with overflow/underflow risk.
- The AFAG method reports better performance in terms of control data than conventional methds such as location map, flag bits, basic flag array, among others. 
- The image qualitiy is slightly affected by the use of AFAG. Nevertheless, the induced distiortion can be reverted.

## Usage
Follow the instructions in the `main.m` file:

``` matlab

%% Example of usage of the AFAG method.
% Load the image (grayscale)
% Image #120 of the BOWS2 dataset ("120.pgm") 
im = imread("test_image.pgm");
figure; imshow(im); 

% Set the maximum modification level (determined by the reversible data
% hiding to be used)
% In this example, the RDH method will modify each pixel either upwards or
% downwards by 2
maxMod = 2; 

% Define a the reserved region. For example, [r, c] means that the first
% 'r'rows and the first 'c' columns are reserved to embedd control 
% information. These will be no processed during the AFAG process.
reservedRegion = [2,2];

% Perform the AFAG process
% AFAG always returns the shorter flag array, that is, the shorter between
% the compressed or the uncompressed version of the flag array.
[imMod, flagArray] = adaptFlagArrayEncode(im, maxMod, reservedRegion);

%% Review the results
figure;
imshow(imMod);

% See the modified pixels
figure;
imshow(xor(im, imMod))

% Print the flag array 
disp("Flag array size (in bits): ")
disp(length(flagArray))

%% Apply reversible data hiding (or watermarking) embedding and extraction
% At this point, the reversible data hiding technique is applied, with no
% overflow issues because they were prevented.
% After extraction of payload and the control information that includes the
% flag array data, the original image can be recovered as follows.

%% Recover the image
imRec = adaptFlagArrayDecode(imMod, flagArray, maxMod, reservedRegion);

%% Verify the results
if (imRec == im)
    disp("Image successfully recovered!")
else
    disp("ERROR - An error has occured.")
end

close all;
```

## Citation
The presented algorithm is product of an published work entitled "An adaptive method for prevention of overflow in reversible data hiding schemes" - DOI: [10.1016/j.eswa.2023.120610](https://doi.org/10.1016/j.eswa.2023.120610)

If you use this method, add the following citation to your paper:
`Bibtex`
```
@article{penuelas2023adaptive,
    title = {An adaptive method for prevention of overflow in reversible data hiding schemes},
    author={Pe{\~n}uelas-Angulo, Alejandro and Feregrino-Uribe, Claudia and Cumplido, Ren{\'e}},
    journal = {Expert Systems with Applications},
    volume = {230},
    pages = {120610},
    year = {2023},
    issn = {0957-4174},
    publisher={Elsevier}
    doi = {https://doi.org/10.1016/j.eswa.2023.120610},
    url = {https://www.sciencedirect.com/science/article/pii/S0957417423011120},
}
```
