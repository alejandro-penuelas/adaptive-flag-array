function [imRecFinal] = adaptFlagArrayDecode(imMod, flgArray, maxMod, reservedRegion)
%adaptFlagArrayEncode This function decodes the flag array of an image adaptively to avoid
%   the overflow of the image.

%% Initial processing

% Detecting the maximum gray level for the image format
if isa(imMod, 'uint8')
    maxGreyLvl = 2^8-1;
elseif isa(imMod, 'uint16')
    maxGreyLvl = 2^16-1;
else
    disp('Unexpected image format. The max gray level can not be selected');
end

% Copy the modified image and resize
imRec = double(imMod(reservedRegion+1:end, reservedRegion+1:end));

% Extraction of binary information in the flgArray
typeCodeBits = 2;                                           % Num of bits for typeCode
typeCode = bi2de(flgArray(1:typeCodeBits)');

limBits = 6;                                                % Num of bits for limit values
limLowIdxs = [typeCodeBits+1, typeCodeBits + limBits];
limHigIdxs = [limLowIdxs(2)+1, limLowIdxs(2) + limBits];

limLow = bi2de(flgArray(limLowIdxs(1): limLowIdxs(2))');
limHig = bi2de(flgArray(limHigIdxs(1): limHigIdxs(2))');

% The rest of locMap is then the final code
finalCode = flgArray(limHigIdxs(2)+1 : end);

%% Decoding stage

if typeCode == 1
    
    % Recovering the aCode from the flgArray
    aCode = finalCode;
    aCodeBin = transpose(reshape(aCode, [8, numel(aCode)/8]));
    aCodeDec = bi2de(aCodeBin);
    locMapCell = arith07(aCodeDec);
    locMap = locMapCell{1}; 
    
elseif typeCode == 2
    
     % Recovering the locMap from the flgArray
     locMap = finalCode;
end

% Recovering the values under and over the limits
imRec(imRec < maxMod + limLow) = imRec(imRec < maxMod + limLow) - maxMod;
imRec(imRec > maxGreyLvl-maxMod-limHig) = ...
    imRec(imRec > maxGreyLvl-maxMod-limHig) + maxMod;

% Recovering the low values and high values with the location map
overLowRec = find(imRec >= maxMod + limLow & imRec <= 2*maxMod-1 + limLow);
overHigRec = ...
    find(imRec <= maxGreyLvl-maxMod-limHig & imRec >= maxGreyLvl-2*maxMod+1 - limHig);
imRec(overLowRec) = imRec(overLowRec) - locMap(1:numel(overLowRec))*maxMod;
imRec(overHigRec) = imRec(overHigRec) + locMap(numel(overLowRec)+1:end)*maxMod;

% Reformat
imRecFinal = double(imMod);
imRecFinal(reservedRegion+1:end, reservedRegion+1:end) = imRec;
imRecFinal = cast(imRecFinal, 'like', imMod);

end
