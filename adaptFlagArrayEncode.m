function [imModFinal, flgArray, rawFlgArray, arithFlgArray, limLow, limHig] =...
    adaptFlagArrayEncode(im, maxMod, reservedRegion)

% function [imModFinal, flgArray, limLow, limHig] = ...
%             adaptFlagArrayEncode(im, maxMod, reservedRegion)

%adaptFlagArrayEncode This function encodes the flag array of an image adaptively to avoid
%   the overflow of the image. Returns the modified image and the required flag array to
%   recover it.
%
% INPUTS:
%   im: Orignal image.
%   maxMod: Maximum modfification for in the image according to the embedding algorthm.
%   limMax: Threshold for the the search of limits.
%   blockSize: Size of the block to crop the modified image and preserve first row and
%       column.
%
% OUTPUTS:
%   imMod: Modified image which can avoid the under/overflow problems.
%   flgArray: Encoded flag array encoded and compressed, if applicable.

% NOTES:
% 1) Currently testing the efects of fixed encoding (raw flag array).

%% Initial processing

% Detecting the maximum gray level for the image format
if isa(im, 'uint8')
    maxGreyLvl = 2^8-1;
elseif isa(im, 'uint16')
    maxGreyLvl = 2^16-1;
else
    disp('Unexpected image format. The max gray level can not be selected');
end

% Number of bits for encoding
typeCodeBits = 2;
% flagArrayLenBits = 18;
limBits = 6;

% Define the limMax for search
limMax = 2^limBits;

%% Finding the limits and Modification of image for locMap generation

[locMap, imModFinal, limLow, limHig] = ...
    afagCoreEncC_mex(im, reservedRegion, limMax, maxMod, maxGreyLvl);

%% Encoding stage

aCode = arithCode(locMap, 'enc');

% Real flag array _____________________________________________________________
if length(aCode) < length(locMap)
    % The arithmetic code is shorter than the location map so we return aCode
    typeCode = 1;
    finalCode = aCode;
else
    % The raw locMap shorter than aCode so we return locMap
    typeCode = 2;
    finalCode = locMap;
end

% Define the flag array
flgArray = [de2bi(typeCode, typeCodeBits)'; ...
            de2bi(limLow, limBits)'; ...
            de2bi(limHig, limBits)'; ...
%             de2bi(length(aCode), flagArrayLenBits); ...
            finalCode];
% ______________________________________________________________________________

% Test code
% Define the raw flag array
typeCode = 2;
rawFlgArray = [de2bi(typeCode, typeCodeBits)'; ...
               de2bi(limLow, limBits)'; ...
               de2bi(limHig, limBits)'; ...
               locMap];

% Define the arith flag array
typeCode = 1;
arithFlgArray = [de2bi(typeCode, typeCodeBits)'; ...
                de2bi(limLow, limBits)'; ...
                de2bi(limHig, limBits)'; ...
                aCode];

end

