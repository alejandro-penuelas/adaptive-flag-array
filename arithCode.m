function [outVector] = arithCode(binaryVector, enc_or_dec)
%arithCode Performs encoding or decoding of binary vectors acording to the selected
% opperation and using the 'arith07'.
%
% Paramemters:
%   binaryVector = Vector in format double filled with a stream of 1s and 0s

% Pre-allocate the outVector to prevent errors
outVector = [];

switch enc_or_dec
    case {'enc', 'Enc', 'ENC'}
        
        % Encode with arithmetic code
        aCodeDec = arith07Enc_mex( {double(binaryVector), []} );
        aCodeBin = de2bi(aCodeDec);
        outVector = reshape(aCodeBin', [numel(aCodeBin), 1]);

    case {'dec', 'Dec', 'DEC'}
        
        % Recovering the aCode from the flgArray
        aCodeBin = transpose(reshape(binaryVector, [8, numel(binaryVector)/8]));
        aCodeDec = bi2de(aCodeBin);
        outVectorCell = arith07(aCodeDec);
        outVector = outVectorCell{1}; 
    
    otherwise
        disp('ERROR - Supported opperations are <enc> for encoding or <dec> for decoding.');
end

end  % End function

