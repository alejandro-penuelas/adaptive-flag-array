%% Example of usage of the AFAG method.
% Load the image (grayscale)
% Image #120 of the BOWS2 dataset ("120.pgm") 
im = imread("test_image.pgm");
figure; imshow(im); 

% Set the maximum modification level (determined by the reversible data
% hiding to be used)
% In this example, the RDH method will modify each pixel either upwards or
% downwards by 2
maxMod = 2; 

% Define a the reserved region. For example, [r, c] means that the first
% 'r'rows and the first 'c' columns are reserved to embedd control 
% information. These will be no processed during the AFAG process.
reservedRegion = [2,2];

% Perform the AFAG process
% AFAG always returns the shorter flag array, that is, the shorter between
% the compressed or the uncompressed version of the flag array.
[imMod, flagArray] = adaptFlagArrayEncode(im, maxMod, reservedRegion);

%% Review the results
figure;
imshow(imMod);

% See the modified pixels
figure;
imshow(xor(im, imMod))

% Print the flag array 
disp("Flag array size (in bits): ")
disp(length(flagArray))

%% Apply reversible data hiding (or watermarking) embedding and extraction
% At this point, the reversible data hiding technique is applied, with no
% overflow issues because they were prevented.
% After extraction of payload and the control information that includes the
% flag array data, the original image can be recovered as follows.

%% Recover the image
imRec = adaptFlagArrayDecode(imMod, flagArray, maxMod, reservedRegion);

%% Verify the results
if (imRec == im)
    disp("Image successfully recovered!")
else
    disp("ERROR - An error has occured.")
end

close all;